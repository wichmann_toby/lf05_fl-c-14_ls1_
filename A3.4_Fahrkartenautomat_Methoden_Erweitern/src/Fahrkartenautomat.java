/* A3.3 Fahrkartenautomat
 * @Autor: Toby Wichmann
 * @Version: 1.4
 * @Change: 1.1: Korrektur 2 Nachkommastellen
 * 			1.2: Anzahl Tickets eingef�gt
 * 			1.3: Methoden Einf�gen
 * 			1.4: Methoden erweitern(Warten & M�nzenauszahlung)
 */
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {

    
       double fahrkartenBestellung = fahrkartenbestellungErfassen();
       double bezahlung = fahrkartenBezahlen(fahrkartenBestellung);
       fahrkartenAusgaben();
       rueckgeldAusgeben(bezahlung, fahrkartenBestellung);
       
      
 
       

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
    
    public static double fahrkartenbestellungErfassen() {
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	double zuZahlenderBetrag;
    	int anzahlTickets;
    	
    	System.out.print("Zu zahlender Betrag (EURO): ");
    	zuZahlenderBetrag = tastatur.nextDouble();
    	System.out.print("\nAnzahl der Tickets: ");  
    	anzahlTickets = tastatur.nextInt();
    	zuZahlenderBetrag *= anzahlTickets;
    	
    	return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlen) {
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag;
    	double zuZahlenderBetrag = zuZahlen;
    	double eingeworfeneM�nze;
    	
			 // Geldeinwurf
			// -----------
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   //System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag) + " Euro");
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	      	   
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
            
        }
        return eingezahlterGesamtbetrag;
    }
    public static void fahrkartenAusgaben() {
    	
    	// Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
       warte();
        }
        System.out.println("\n\n");
    }
    public static void rueckgeldAusgeben(double Gesamtbetrag, double Betrag) { 
    	
    	double r�ckgabebetrag;
    	double eingezahlterGesamtbetrag = Gesamtbetrag;
    	double zuZahlenderBetrag = Betrag;
    	 	
    	
    	// R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
     	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
         	  muenzeAusgeben(2, "EURO");
 	          r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
            	muenzeAusgeben(1, "EURO");
 	          r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
            	muenzeAusgeben(50, "CENT");
 	          r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
            	muenzeAusgeben(20, "CENT");
  	          r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
            	muenzeAusgeben(10, "CENT");
 	          r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
            	muenzeAusgeben(5, "CENT");
  	          r�ckgabebetrag -= 0.05;
            }
        }
    }
    
    public static void warte() {
    	
    	try {
    		Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    }
    public static void muenzeAusgeben(int betrag, String einheit) {
    	
    	System.out.println(betrag + " " + einheit);
    	
    }
}