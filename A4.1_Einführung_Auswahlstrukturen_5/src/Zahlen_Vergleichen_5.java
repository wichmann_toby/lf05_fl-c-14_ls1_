/*A4.1 Zahlen_Vergleichen_5
 *@Autor Toby Wichmann 
 *Version 1.0
 */

import java.util.Scanner;

public class Zahlen_Vergleichen_5 {

	public static void main(String[] args) {
		
		int eingabe1;
		int eingabe2;
		int eingabe3;
		
		
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben sie eine ganze Zahl ein: ");
		eingabe1 = myScanner.nextInt();
		
		System.out.print("\n\nBitte geben sie eine zweite ganze Zahl ein: ");
		eingabe2 = myScanner.nextInt();
		
		System.out.print("\n\nBitte geben sie eine dritte ganze Zahl ein: ");
		eingabe3 = myScanner.nextInt();
			
		
		if(eingabe3 > eingabe2 || eingabe3 > eingabe1) {
			System.out.println("\nDie dritte eingegebene Zahl ("+ eingabe3 +") ist gr��er als die zweite eingegebene Zahl ("+ eingabe2 +") oder als die erste eingegebene Zahl ("+ eingabe1 +")!");
		}
	
	}

}
