/*A4.1 Zahlen_Vergleichen_3
 *@Autor Toby Wichmann 
 *Version 1.0
 */

import java.util.Scanner;

public class Zahlen_Vergleichen_3 {

	public static void main(String[] args) {
		
		int eingabe1;
		int eingabe2;
		
		
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben sie eine ganze Zahl ein: ");
		eingabe1 = myScanner.nextInt();
		
		System.out.print("\n\nBitte geben sie eine zweite ganze Zahl ein: ");
		eingabe2 = myScanner.nextInt();
			
		
		if(eingabe1 > eingabe2 || eingabe1 == eingabe2) {
			System.out.println("\nDie erste eingegebene Zahl ("+ eingabe1 +") ist gr��er oder gleich wie die zweite eingegebene Zahl ("+ eingabe2 +")!");
		}else {
			System.out.print("die eingegebenen Zahlen sind nicht gleich oder Zahl 1 ist nicht gr��er als Zahl 2");
		}
	
	}

}
