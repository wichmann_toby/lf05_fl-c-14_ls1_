/*A3.1 Einf�hrung in Methoden - Quadrieren
 * @Autor: Toby Wichmann
 * @Version: 1.0
*/
public class Quadrieren {
   
	public static void main(String[] args) {
		
		double x;
		double ergebnis;
		
		
		x = Eingabe();
		ergebnis = Verarbeitung(x);
		Titel();
		Ausgabe(x, ergebnis);
	
	}
	
	public static double Eingabe() {
		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================
		return 5;
	}
	
	public static void Titel() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
		
	}
	
	public static double Verarbeitung(double x) {
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		double ergebnis= x * x;
		return ergebnis;
	}
	
	public static void Ausgabe(double x, double ergebnis) {				
		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
		
	}
		
}
