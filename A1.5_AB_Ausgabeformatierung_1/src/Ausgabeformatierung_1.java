// A1.5 Ausgabeformatierung 1 - Toby Wichmann
public class Ausgabeformatierung_1 {

	public static void main(String[] args) {
		
		int alter = 16; 
		String name = "Toby";
		String trenner = "------------------";
		
		
		System.out.println("Ich bin der erste Satz.");
		System.out.print("Ich bin der zweite Satz.");
		System.out.println("\r------------------");
		
		System.out.print("\rIch bin der \"dritte\" Satz \rmit einem Umbruch.");
		System.out.println("\r------------------");
		
		System.out.println("Ich hei�e " + name + " und bin " + alter + " alt.");
		
		// print: Schreibt alles in einer Zeile. 
		// Println: Setzt am ende einer Zeile ein Umbruch.
		
		System.out.println(trenner);
		
		//Aufgabe 2
		System.out.printf("%10.1s\n","****************************");
		System.out.printf("%11.3s\n","****************************");
		System.out.printf("%12.5s\n","****************************");
		System.out.printf("%13.7s\n","****************************");
		System.out.printf("%14.9s\n","*****************************");
		System.out.printf("%15.11s\n","***************************");
		System.out.printf("%16.13s\n","**********************");
		System.out.printf("%17.15s\n","********************");
		System.out.printf("%11.3s\n","***********");
		System.out.printf("%11.3s\n","***********");
		
		System.out.println(trenner);
		
		// Aufgabe 3
		System.out.printf("%.2f\n", 22.4234234);
		System.out.printf("%.2f\n", 111.2222);
		System.out.printf("%.2f\n", 4.0);
		System.out.printf("%.2f\n", 1000000.551);
		System.out.printf("%.2f\n", 97.43);
	}
}
