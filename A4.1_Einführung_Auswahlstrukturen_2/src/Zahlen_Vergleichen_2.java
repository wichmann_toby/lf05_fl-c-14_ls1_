/*A4.1 Zahlen_Vergleichen_2
 *@Autor Toby Wichmann 
 *Version 1.0
 */

import java.util.Scanner;

public class Zahlen_Vergleichen_2 {

	public static void main(String[] args) {
		
		int eingabe1;
		int eingabe2;
		
		
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben sie eine ganze Zahl ein: ");
		eingabe1 = myScanner.nextInt();
		
		System.out.print("\n\nBitte geben sie eine zweite ganze Zahl ein: ");
		eingabe2 = myScanner.nextInt();
			
		
		if(eingabe2 > eingabe1) {
			System.out.println("\nDie zweite eingegebene Zahl ("+ eingabe2 +") ist gr��er als die Erste ("+ eingabe1 +")!");
		}
	
	}

}
