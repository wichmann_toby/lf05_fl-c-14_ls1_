/*A4.1 Zahlen_Vergleichen_4
 *@Autor Toby Wichmann 
 *Version 1.0
 */

import java.util.Scanner;

public class Zahlen_Vergleichen_4 {

	public static void main(String[] args) {
		
		int eingabe1;
		int eingabe2;
		int eingabe3;
		
		
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben sie eine ganze Zahl ein: ");
		eingabe1 = myScanner.nextInt();
		
		System.out.print("\n\nBitte geben sie eine zweite ganze Zahl ein: ");
		eingabe2 = myScanner.nextInt();
		
		System.out.print("\n\nBitte geben sie eine dritte ganze Zahl ein: ");
		eingabe3 = myScanner.nextInt();
			
		
		if(eingabe1 > eingabe2 &&  eingabe1 > eingabe3) {
			System.out.println("\nDie erste eingegebene Zahl ("+ eingabe1 +") ist gr��er als die zweite eingegebene Zahl ("+ eingabe2 +") und als die dritte eingegebene Zahl ("+ eingabe3 +")!");
		}
	
	}

}
