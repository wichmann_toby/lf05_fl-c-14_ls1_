/*A4.1 Zahlen_Vergleichen_6
 *@Autor Toby Wichmann 
 *Version 1.0
 */

import java.util.Scanner;

public class Zahlen_Vergleichen_6 {

	public static void main(String[] args) {
		
		int eingabe1;
		int eingabe2;
		int eingabe3;
		
		
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben sie eine ganze Zahl ein: ");
		eingabe1 = myScanner.nextInt();
		
		System.out.print("\n\nBitte geben sie eine zweite ganze Zahl ein: ");
		eingabe2 = myScanner.nextInt();
		
		System.out.print("\n\nBitte geben sie eine dritte ganze Zahl ein: ");
		eingabe3 = myScanner.nextInt();
			
		
		if(eingabe1 > eingabe2 && eingabe1 > eingabe3) {
			System.out.println("\nDie erste eingegebene Zahl ("+ eingabe1 +") ist am gr��ten!");
		}else{
			if(eingabe2 > eingabe1 && eingabe2 > eingabe3) {
				System.out.println("\nDie zweite eingegebene Zahl ("+ eingabe2 +") ist am gr��ten!");
			}else{
				if(eingabe3 > eingabe2 && eingabe3 > eingabe1) {
					System.out.println("\nDie dritte eingegebene Zahl ("+ eingabe3 +") ist am gr��ten!");
				}
			}
		}
	
	}

}
