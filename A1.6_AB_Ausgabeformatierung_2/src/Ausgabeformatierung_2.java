//A1.6 AB Ausgabeformatierung 2 - Toby Wichmann
public class Ausgabeformatierung_2 {

	public static void main(String[] args) {
		
		String trenner ="------------------------------";
		
		// Aufgabe 1
		System.out.printf("%11.2s\n","**");
		System.out.printf("%8.1s","*");
		System.out.printf("%5.1s\n","*");
		System.out.printf("%8.1s","*");
		System.out.printf("%5.1s\n","*");
		System.out.printf("%11.2s\n","**");
		
		System.out.println(trenner);
		
		//Aufgabe 2
		
		System.out.printf("%-5s","0!");
		System.out.print("=");
		System.out.printf("%-19s","");
		System.out.print("=");
		System.out.printf("%4.4s\n",1);
		
		System.out.printf("%-5s","1!");
		System.out.print("=");
		System.out.printf("%-19s"," 1");
		System.out.print("=");
		System.out.printf("%4.4s\n",1);
		
		System.out.printf("%-5s","2!");
		System.out.print("=");
		System.out.printf("%-19s"," 1 * 2");
		System.out.print("=");
		System.out.printf("%4.4s\n",1*2);
		
		System.out.printf("%-5s","3!");
		System.out.print("=");
		System.out.printf("%-19s"," 1 * 2 * 3");
		System.out.print("=");
		System.out.printf("%4.4s\n",1*2*3);
		
		System.out.printf("%-5s","4!");
		System.out.print("=");
		System.out.printf("%-19s"," 1 * 2 * 3 * 4");
		System.out.print("=");
		System.out.printf("%4.4s\n",1*2*3*4);
		
		System.out.printf("%-5s","5!");
		System.out.print("=");
		System.out.printf("%-19s"," 1 * 2 * 3 * 4 * 5");
		System.out.print("=");
		System.out.printf("%4.4s\n",1*2*3*4*5);
		
		System.out.println(trenner);
		System.out.println("\n");
		
		// Aufgabe 3
		System.out.printf("%-12s","Fahrenheit");
		System.out.printf("%1s","|");
		System.out.printf("%10s\n","Celsius");
		System.out.printf("------------------------\n");
		
		System.out.printf("%+-12d",-20);
		System.out.printf("%1s","|");
		System.out.printf("%10.2f\n",-28.8889);
		
		System.out.printf("%+-12d",-10);
		System.out.printf("%1s","|");
		System.out.printf("%10.2f\n",-23.3333);
		
		System.out.printf("%+-12d",0);
		System.out.printf("%1s","|");
		System.out.printf("%10.2f\n",-17.7778);
		
		System.out.printf("%+-12d",20);
		System.out.printf("%1s","|");
		System.out.printf("%10.2f\n",-6.6667);
		
		System.out.printf("%+-12d",30);
		System.out.printf("%1s","|");
		System.out.printf("%10.2f\n",-1.1111);
	}

}
