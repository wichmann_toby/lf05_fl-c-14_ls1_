/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 09.01.2022
  * @author  Toby Wichmann
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 9;
    
    // Anzahl der Sterne in unserer Milchstra�e
    String anzahlSterne = "9.000.000.000";
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3645000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    short alterTage = 6205;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    String gewichtKilogramm = "150.000 kg";
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    String flaecheGroessteLand = "17.098.242 km�";

    
    // Wie gro� ist das kleinste Land der Erde?
    
    String  flaecheKleinsteLand = "0,44 km�";
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);    
    System.out.println("Anzahl der Sterne: "+ anzahlSterne);   
    System.out.println("Einwohner Anzahl Berlin: "+ bewohnerBerlin);   
    System.out.println("Alter in Tagen sind: "+ alterTage);     
    System.out.println("Gewicht in KG vom Schw�rsten Tier der Welt: "+ gewichtKilogramm);    
    System.out.println("Fl�che des gr��ten Landes der Erde: "+ flaecheGroessteLand);    
    System.out.println("Fl�che Kleinste Land: "+ flaecheKleinsteLand);   
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
