/*A4.1 Wenn-Dann-Aktivit�ten 
 *@Autor Toby Wichmann 
 *Version 1.0
 */

import java.util.Scanner;

public class Auswahlstrukturen {

	public static void main(String[] args) {
		
		int eingabe;
		
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("1. Wenn ich Hunger habe muss ich essen.");
		System.out.println("2. Wenn ich m�de bin muss ich Arbeiten gehen.");
		
		System.out.print("\nBitte w�hlen sie eine Zahl: ");
		
		eingabe = myScanner.nextInt();
		
		if(eingabe == 1) {
			System.out.println("\nDas stimmt. Wenn ich Hunger habe muss ich etwas essen.");			
		}
		
		if(eingabe == 2) {
			System.out.println("\nNein das stimmt nicht. Wenn ich m�de bin muss ich schlafen.");
			
		}
		
		if(eingabe < 1) {
			System.out.println("\nDiese Auswahl steht nicht zur Verf�gung!");
		}		
		
		if(eingabe > 2) {
			System.out.println("\nDiese Auswahl steht nicht zur Verf�gung!");
		}
	
	}

}
